# React-Redux setup with 16.4v

To be able to run/build the web application, Node.js (https://nodejs.org/en/) must be installed.

---

Before running/building the web application, run the command _**'npm install'**_

---

**To** run the application in development mode use command _**'npm start'**_

---

**To** Build the distribution use command _**'npm run build'**_

---

**To** run application, open build/index.html into browser or host dist folder on web server and access dist/index.html.

---
