import React from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionMaster from '../../actions/uiAction';

class Page2 extends React.Component {
  constructor() {
    super();
  }

  render() {
    // No need to wrap list items in an extra element!
    return [
      // Don't forget the keys :)
      <li key="A">First item</li>,
      <li key="B">Second item</li>,
      <li key="C">Third item</li>
    ];
  }
}

function mapStateToProps(state, ownProps) {
  return state.application;
}

function mapDispatchToProps(dispatch) {
  return {
    actionMaster: bindActionCreators(actionMaster, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Page2);
