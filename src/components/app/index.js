import React from 'react';
import { Route, Link } from 'react-router-dom';
import Welcome from '../home/welcome';
import About from '../about/about';
import Page2 from '../page2/page2';
import Page3 from '../page3/page3';
import Page4 from '../page4/page4';
import DetailViewPage from '../DetailsView/DetailsViewPage';
import ProductForm from '../FormMaterialUi/Form';

const App = () => (
  <div>
    <main>
      <Route exact path="/" component={Welcome} />
      <Route exact path="/about-us" component={About} />
      <Route exact path="/Page2" component={Page2} />
      <Route exact path="/Page3" component={Page3} />
      <Route exact path="/Page4" component={Page4} />
      <Route exact path="/DetailViewPage" component={DetailViewPage} />
      <Route exact path="/ProductForm" component={ProductForm} />
    </main>
  </div>
);

export default App;
