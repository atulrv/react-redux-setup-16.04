import React from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionMaster from '../../actions/uiAction';
import { Link } from 'react-router-dom';
import Header from '../common/Header';
import PropTypes from 'prop-types';
import Footer from '../common/Footer';
import PreLoader from '../common/preloader';

class Welcome extends React.Component {
  constructor() {
    super();
  }

  componentDidMount() {
    this.props.actionMaster.startLoader();
    setTimeout(() => this.props.actionMaster.getStudentData(), 2000);
  }

  onMove = data => {
    setTimeout(
      () => this.props.actionMaster.onGoToDetail(data),
      this.props.history.push('/DetailViewPage'),
      2000
    );
    //this.props.history.push('/about-us');
  };

  render() {
    console.log('This.props', this.props);
    return (
      <div>
        <Header />

        <div className="container">
          <div className="row">
            {this.props.preLoader === true ? (
              <div className="loaderH">
                <PreLoader />
              </div>
            ) : null}

            {this.props.studentData !== null ||
            this.props.studentData !== undefined ? (
              this.props.studentData.map((data, i) => {
                return (
                  <div
                    className="col-sm-6 col-lg-4"
                    onClick={() => this.onMove(data)}>
                    <div className="panel panel-primary">
                      <div className="panel-heading">{data.name}</div>
                      <div className="panel-body">
                        <div className="panel-footer">
                          Roll No.<span>{data.rollNo}</span>
                        </div>
                        <div className="panel-footer">
                          Total Marks<span>
                            {data.marks.s1 + data.marks.s2 + data.marks.s3}
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })
            ) : (
              <div>No Data!</div>
            )}
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return state.application;
}

function mapDispatchToProps(dispatch) {
  return {
    actionMaster: bindActionCreators(actionMaster, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Welcome);
