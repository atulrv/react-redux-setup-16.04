import React from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionMaster from '../../actions/uiAction';
import Page2 from '../page2/page2';
import Page3 from '../page3/page3';

class Page4 extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <React.Fragment>
        <Page2 />
        <Page3 />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return state.application;
}

function mapDispatchToProps(dispatch) {
  return {
    actionMaster: bindActionCreators(actionMaster, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Page4);
