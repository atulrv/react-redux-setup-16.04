import React from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionMaster from '../../actions/uiAction';

class Page3 extends React.Component {
  constructor() {
    super();
  }

  render() {
    return 'Look ma, no spans!';
  }
}

function mapStateToProps(state, ownProps) {
  return state.application;
}

function mapDispatchToProps(dispatch) {
  return {
    actionMaster: bindActionCreators(actionMaster, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Page3);
